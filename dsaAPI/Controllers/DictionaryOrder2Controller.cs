﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dsaAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/DictionaryOrder2")]
    public class DictionaryOrder2Controller : Controller
    {
        /// <summary>
        /// 取得固定數量的所有組合(ex: abc,bca算同一組)
        /// </summary>
        /// <param name="count"></param>
        /// <param name="chars"></param>
        /// <returns></returns>
        [HttpPost("{count}")]
        public List<string> Post(int count, [FromBody]char[] chars)
        {
            var result = new List<string>();
            try
            {
                Action<string, int> myFunc = null;
                myFunc = (s, i) =>
                {
                    s = s + chars[i];
                    if (s.Length == count)
                    {
                        result.Add(s);
                    }
                    else
                    {
                        int _index = i + 1;
                        if (_index < chars.Length)
                        {
                            for (int j = _index; j <= chars.Length - count + s.Length; j++)
                            {
                                myFunc(s, j);
                            }
                        }
                    }
                };

                for (int i = 0; i <= chars.Length - count; i++)
                {
                    myFunc("", i);
                }
            }
            catch (Exception ex)
            {
                result.Add(ex.Message);
            }

            return result;
        }
    }
}