﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dsaAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Hanoi")]
    public class HanoiController : Controller
    {
        [HttpGet("{count}")]
        public List<string> Get(int count)
        {
            var result = new List<string>();

            try
            {
                Action<int, string, string, string> myFunc = null;
                myFunc = (n, f, c, t) =>
                {
                    if (n > 1)
                    {
                        myFunc(n - 1, f, t, c);
                        myFunc(1, f, c, t);
                        myFunc(n - 1, c, f, t);
                    }
                    else
                    {
                        result.Add(string.Format("{0} to {1}", f, t));
                    }
                };

                myFunc(count, "A", "B", "C");
            }
            catch (Exception ex)
            {
                result.Add(ex.Message);
            }

            return result;
        }
    }
}