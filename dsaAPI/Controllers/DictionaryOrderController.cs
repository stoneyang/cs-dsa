﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dsaAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/DictionaryOrder")]
    public class DictionaryOrderController : Controller
    {
        /// <summary>
        /// 取得所有組合
        /// </summary>
        /// <param name="chars"></param>
        /// <returns></returns>
        [HttpPost]
        public List<string> Post([FromBody]char[] chars)
        {
            var result = new List<string>();
            try
            {
                Action<string, int> myFunc = null;
                myFunc = (s, i) =>
                {
                    s = s + chars[i];
                    result.Add(s);
                    if (i + 1 < chars.Length)
                    {
                        for (int j = i + 1; j < chars.Length; j++)
                        {
                            myFunc(s, j);
                        }
                    }
                };

                for (int i = 0; i < chars.Length; i++)
                {
                    myFunc("", i);
                }
            }
            catch (Exception ex)
            {
                result.Add(ex.Message);
            }

            return result;
        }
    }
}