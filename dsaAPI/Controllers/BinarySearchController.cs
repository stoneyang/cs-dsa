﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dsaAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/BinarySearch")]
    public class BinarySearchController : Controller
    {
        [HttpPost("{target}")]
        public int Post(int target, [FromBody] int[] nums)
        {
            int result = 0;
            
            try
            {
                Func<int, int, int> myFunc = null;
                myFunc = (start, end) =>
                {
                    if(start > end){
                        return -1;
                    }

                    var mid = start + Convert.ToInt16(Math.Ceiling((decimal)(end - start) / 2));
                    var current = nums[mid];
                    if (current == target)
                    {
                        return mid;
                    }
                    else if (current > target) {
                        return myFunc(start, mid - 1);                                               
                    }
                    else
                    {
                        return myFunc(mid  + 1, end);                         
                    }
                };

                result = myFunc(0, nums.Length-1);

            }
            catch (Exception ex)
            {
            }

            return result;
        }
    }
}