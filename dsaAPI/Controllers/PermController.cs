﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dsaAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Perm")]
    public class PermController : Controller
    {
        [HttpPost("{count}")]
        public List<string> Post(int count, [FromBody]List<char> chars)
        {
            var result = new List<string>();

            try
            {
                Action<string, string> myFunc = null;
                myFunc = (s, c) =>
                {
                    s = s + c;
                    if (s.Length == count)
                    {
                        result.Add(s);
                    }
                    else
                    {
                        for (int i = 0; i < chars.Count; i++)
                        {
                            if (!s.Contains(chars[i]))
                            {
                                myFunc(s, chars[i].ToString());
                            }
                        }
                    }
                };

                myFunc("", "");
            }
            catch (Exception ex)
            {
                result.Add(ex.Message);
            }

            return result;
        }
    }
}