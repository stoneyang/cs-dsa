﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dsaAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/IntegerBreak")]
    public class IntegerBreakController : Controller
    {
        [HttpGet("{n}")]
        public long Get(int n)
        {
            if (n == 2)
                return 1;

            if (n == 3)
                return 2;

            var residue = n % 3;

            if (residue == 0)
                return (long)Math.Pow(3, (n / 3));
            else if (residue == 1)
            {
                var a = (n - 4) / 3;
                return (long)Math.Pow(3, a) * 4;
            }
            else
            {
                var a = (n - 2) / 3;
                return (long)Math.Pow(3, a) * 2;
            }
        }


        /// <summary>
        /// https://leetcode.com/problems/integer-break/description/
        /// 參考IntTakeController
        /// </summary>
        /// <param name="current"></param>
        /// <returns></returns>
        //[HttpGet("{current}")]
        //public int Get(int current)
        //{
        //    //var totalLists = new List<List<int>>();
        //    var max = 0;

        //    Action<int, int, int> myFunc = null;
        //    myFunc = (num, xnum, sum) =>
        //    {
        //        int quotient = sum / 2;

        //        for (int i = num; i <= quotient; i++)
        //        {
        //            if (sum - i >= num)
        //            {
        //                var _xnum = xnum * (sum - i) * i;
        //                if(_xnum > max)
        //                {
        //                    max = _xnum;
        //                }
                        
        //                if (sum - i >= num)
        //                {
        //                    var _newnum = xnum * i;
        //                    myFunc(i, _newnum, sum - i);
        //                }
        //            }
        //        }
        //    };

        //    myFunc(1, 1, current);


        //    return max;
        //}
    }
}