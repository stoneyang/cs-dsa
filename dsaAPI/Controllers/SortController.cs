﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace dsaAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/Sort")]
    public class SortController : Controller
    {
        [HttpPost]
        public int[] Post([FromBody]int[] ints)
        {
            int[] result = null;

            try
            {
                var slist = new int[ints.Length];

                for (int i = 0; i < ints.Length; i++)
                {
                    slist[i] = ints[i];
                    for (int j = i; j > 0; j--)
                    {
                        if (slist[j] < slist[j - 1])
                        {
                            int temp = slist[j];
                            slist[j] = slist[j - 1];
                            slist[j - 1] = temp;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                result = slist;
            }
            catch (Exception ex)
            {

            }

            return result;
        }
    }
}